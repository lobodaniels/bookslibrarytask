﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookLibraryWeb;
using BookLibraryData;

namespace BookLibraryWeb.Controllers
{
    /// <summary>
    /// Book Library Controller Class
    /// </summary>
    public class BookLibraryController : Controller
    {
        private static IEnumerable<SelectListItem> items = new List<SelectListItem>();

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View((new BookLibraryDBContext()).GetAllBooks());
        }

        /// <summary>
        /// Searches the by author.
        /// </summary>
        /// <param name="book">The book.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchByAuthor(Book book)
        {
            using (var model = new BookLibraryDBContext())
            {
                try
                {
                    return View(model.GetBooksByAuthor(book.Author).ToList());
                }
                catch
                {
                    return View();
                }
            }
        }

        /// <summary>
        /// Searches the by author.
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchByAuthor()
        {
            return View();
        }

        /// <summary>
        /// Sets the type of the category.
        /// </summary>
        private void SetCategoryType()
        {
            if (items.Count() == 0)
            {
                using (var model = new BookLibraryDBContext())
                {
                    IList<Category> values = model.Categories.ToList();
                    items = from value in values
                            select new SelectListItem
                            {
                                Text = value.Name,
                                Value = value.Id.ToString(),
                            };
                }
            }

            ViewBag.CategoryType = items;
        }

        /// <summary>
        /// Searches the by category.
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchByCategory()
        {
            this.SetCategoryType();
            return View();
        }

        /// <summary>
        /// Categories the chosen.
        /// </summary>
        /// <param name="CategoryType">Type of the category.</param>
        /// <returns></returns>
        public ViewResult CategoryChosen(string CategoryType)
        {
            this.SetCategoryType();
            using (var model = new BookLibraryDBContext())
            {
                int id = 0;
                if (int.TryParse(CategoryType, out id))
                {
                    return View("SearchByCategory", model.GetBooksByCategory(id).ToList());
                }
            }

            return View();
        }

        /// <summary>
        /// Authors the search.
        /// </summary>
        /// <param name="author">The author.</param>
        /// <returns></returns>
        public ActionResult AuthorSearch(string author)
        {
            using (var model = new BookLibraryDBContext())
            {
                try
                {
                    return View("SearchByAuthor", model.GetBooksByAuthor(author));
                }
                catch
                {
                    return View();
                }
            }
        }

    }
}

﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BookLibraryData
{
    /// <summary>
    /// Entity Framework context class
    /// </summary>
    public class BookLibraryDBContext : DbContext
    {
        public BookLibraryDBContext()
            : base("BookLibraryDB")
        {

        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Gets the category by id.
        /// </summary>
        /// <param name="catId">The cat id.</param>
        /// <returns></returns>
        public Category GetCategoryById(int catId){
            using (BookLibraryDBContext context = new BookLibraryDBContext())
            {
                return context.Categories.Where(x => x.Id == catId).Single();
            }
        }


        /// <summary>
        /// Gets all books.
        /// </summary>
        /// <returns></returns>
        public IList<Book> GetAllBooks()
        {
            using (BookLibraryDBContext context = new BookLibraryDBContext())
            {
                return context.Books.AsQueryable().Include(x => x.Category).ToList();
            }
        }

        /// <summary>
        /// Gets the books by author.
        /// </summary>
        /// <param name="author">The author.</param>
        /// <returns></returns>
        public IList<Book> GetBooksByAuthor(string author)
        {
            using (BookLibraryDBContext context = new BookLibraryDBContext())
            {
                return context.Books.Where(x => x.Author.StartsWith(author)).Include(x => x.Category).ToList();
            }
        }

        /// <summary>
        /// Gets the books by category.
        /// </summary>
        /// <param name="categoryId">The category id.</param>
        /// <returns></returns>
        public IList<Book> GetBooksByCategory(int categoryId)
        {
            using (BookLibraryDBContext context = new BookLibraryDBContext())
            {
                return context.Books.Where(x => x.Category.Id.Equals(categoryId)).Include(x => x.Category).ToList();
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace BookLibraryData
{
    /// <summary>
    /// Book  entity class
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Book" /> class.
        /// </summary>
        public Book()
        {
        }

        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Author { get; set; }

        public Category Category { get; set; }
    }
}

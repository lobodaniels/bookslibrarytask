﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BookLibraryData
{
    /// <summary>
    /// Category entity class
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Category" /> class.
        /// </summary>
        public Category()
        {

        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

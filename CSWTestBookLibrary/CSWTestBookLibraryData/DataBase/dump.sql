USE [master]
GO
/****** Object:  Database [BookLibraryDB]    Script Date: 16/07/2015 6:56:42 ******/
CREATE DATABASE [BookLibraryDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BookLibraryDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BookLibraryDB.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BookLibraryDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BookLibraryDB_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BookLibraryDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BookLibraryDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BookLibraryDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BookLibraryDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BookLibraryDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BookLibraryDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BookLibraryDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BookLibraryDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [BookLibraryDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BookLibraryDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BookLibraryDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BookLibraryDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BookLibraryDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BookLibraryDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BookLibraryDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BookLibraryDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BookLibraryDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BookLibraryDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BookLibraryDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BookLibraryDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BookLibraryDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BookLibraryDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BookLibraryDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BookLibraryDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BookLibraryDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BookLibraryDB] SET  MULTI_USER 
GO
ALTER DATABASE [BookLibraryDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BookLibraryDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BookLibraryDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BookLibraryDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BookLibraryDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BookLibraryDB]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 16/07/2015 6:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Author] [nvarchar](max) NOT NULL,
	[Category_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Books] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 16/07/2015 6:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (1, N'The Lion, the Witch and the Wardrobe', N'C. S. Lewis', 1)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (2, N'The Da Vinci Code', N'Dan Brown', 1)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (3, N'Think and Grow Rich', N'Napoleon Hill', 2)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (4, N'The Alchemist', N'Paulo Coelho', 3)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (5, N'Harry Potter and the Prisoner of Azkaban', N'J. K. Rowling', 1)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (6, N'Cien a�os de soledad', N'Gabriel Garc�a M�rquez', 3)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (7, N'The Hobbit', N'J. R. R. Tolkien', 1)
INSERT [dbo].[Books] ([Id], [Title], [Author], [Category_Id]) VALUES (8, N'A Tale Of Two Cities', N'Charles Dickens', 3)
SET IDENTITY_INSERT [dbo].[Books] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Name]) VALUES (1, N'Fiction')
INSERT [dbo].[Categories] ([Id], [Name]) VALUES (2, N'Comedy')
INSERT [dbo].[Categories] ([Id], [Name]) VALUES (3, N'Novel')
SET IDENTITY_INSERT [dbo].[Categories] OFF
/****** Object:  Index [IX_Category_Id]    Script Date: 16/07/2015 6:56:42 ******/
CREATE NONCLUSTERED INDEX [IX_Category_Id] ON [dbo].[Books]
(
	[Category_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Books_dbo.Categories_Category_Id] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_dbo.Books_dbo.Categories_Category_Id]
GO
USE [master]
GO
ALTER DATABASE [BookLibraryDB] SET  READ_WRITE 
GO
